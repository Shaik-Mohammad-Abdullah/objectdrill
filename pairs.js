function pairs(obj) {
	arr = [];
	for (let key in obj) {
		arr.push([key, obj[key]]);
	}
	return arr;
}

module.exports = pairs;
