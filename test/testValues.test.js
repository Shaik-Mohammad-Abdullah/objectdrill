// const { expect } = require("@jest/globals");
const data = require("../data");
const values = require("../values");

const expectedOutput = ['Bruce Wayne', 36, 'Gotham'];

test("Will give an Array of values in string format", () => {
  expect(values(data)).toStrictEqual(expectedOutput);
});
