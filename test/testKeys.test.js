const { expect } = require("@jest/globals");
const data = require("../data");
const keys = require("../keys");

const expectedOutput = [ 'name', 'age', 'location' ];

test("Will give an Array of keys in string format", () => {
  expect(keys(data)).toStrictEqual(expectedOutput);
});
