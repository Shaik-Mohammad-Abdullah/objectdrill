const { expect } = require("@jest/globals");
const data = require('../data');
const map = require('../mapObject');

const expectedOutput = {
    name: 'Bruce Wayne i am added',
    age: '36 i am added',
    location: 'Gotham i am added'
}

test("Will give the transformed array", () => {
    expect(map.mapObject(data, map.cb)).toStrictEqual(expectedOutput);
});
