const { expect } = require("@jest/globals");
const defaults = require('../defaults');
const data = require('../data');
const defaultProps = {
    "age": 40,
    "Pincode": 12821
};

const expectedOutput = { name: 'Bruce Wayne', age: 36, location: 'Gotham', Pincode: 12821 }

test("Will add the array adding Pincode property", () => {
    expect(defaults(data, defaultProps)).toStrictEqual(expectedOutput);
});
