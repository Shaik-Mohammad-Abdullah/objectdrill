const { expect } = require("@jest/globals");
const data = require("../data");
const pairs = require("../pairs");

const expectedOutput = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];

test("Will give an Array of (key, value) pairs", () => {
	expect(pairs(data)).toStrictEqual(expectedOutput);
});
