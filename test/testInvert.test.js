const { expect } = require("@jest/globals");
const invert = require('../invert');
const data = require('../data');

const expectedOutput = { '36': 'age', 'Bruce Wayne': 'name', Gotham: 'location' };

test("Will invert the keys to values and vice-versa", () => {
    expect(invert(data)).toStrictEqual(expectedOutput);
});
