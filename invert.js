function invert(obj) {
    const copyObject = {};
    // console.log("Before invert method")
    // console.log(obj);
    let value = 0;
    for (let key in obj) {
        value = obj[key];
        copyObject[value] = key;
    }
    // console.log("After invert method")
    return copyObject;
}

module.exports = invert;
