function mapObject(data, cb) {
	// console.log("Before cb invoke");
	let copyObject = {};
	// console.log(data);
	for (let key in data) {
		copyObject[key] = data[key];
		copyObject[key] = cb(copyObject[key]);
	}
	return copyObject;
}

function cb(data) {
	// console.log("After cb invoke")
	return data + " i am added";
}

module.exports = { mapObject, cb };
